FROM fedora:33

RUN dnf update -y
RUN dnf install -y python3-pip git
RUN pip3 install jupyterlab

WORKDIR /src
RUN git clone https://gitlab.com/mergetb/xir
WORKDIR /src/xir
RUN git checkout v1-ry
WORKDIR /src/xir/v0.3/mx
RUN pip3 install .

WORKDIR /usr/local/bin
RUN curl -OL https://gitlab.com/mergetb/portal/cli/-/jobs/1028551582/artifacts/raw/mrg
RUN chmod +x mrg

RUN useradd -ms /bin/bash murphy
USER murphy
WORKDIR /home/murphy

ENTRYPOINT jupyter-lab
