import { IRenderMime } from '@jupyterlab/rendermime-interfaces';

//import { JSONObject } from '@lumino/coreutils';

import { Widget } from '@lumino/widgets';

/*
*/
import 'setimmediate';
import FA2Layout from "graphology-layout-forceatlas2/worker";
import { WebGLRenderer } from 'sigma';
import randomLayout from "graphology-layout/random";
import { MultiGraph } from "graphology";

/**
 * The default mime type for the extension.
 */
const MIME_TYPE = 'application/json';

/**
 * The class name added to the extension.
 */
const CLASS_NAME = 'mimerenderer-json';

/**
 * A widget for rendering json.
 */
export class OutputWidget extends Widget implements IRenderMime.IRenderer {
  /**
   * Construct a new output widget.
   */
  constructor(options: IRenderMime.IRendererOptions) {
    super();
    this._mimeType = options.mimeType;
    this.addClass(CLASS_NAME);
  }

  /**
   * Render json into this widget's node.
   */
  renderModel(model: IRenderMime.IMimeModel): Promise<void> {
    
    const data = model.data[this._mimeType] as any; //as JSONObject;
    console.log(data);

    const graph = new MultiGraph();

    if (data.nodes !== undefined) {

        for (const x of data.nodes) {
            let color = "#ccc"
            let size = 5
            if (x.viz !== undefined) {
                if (x.viz.color !== undefined) {
                    color = x.viz.color;
                }
                if (x.viz.size !== undefined) {
                    size = x.viz.size;
                }
            }
            graph.addNode(x.id, {label: x.id, size: size, color: color});
        }

        let mpl_count = 0;

        for (const x of data.links) {
            if(x.endpoints.length == 2) {
                graph.addEdge(
                x.endpoints[0].socket.element,
                x.endpoints[1].socket.element,
                );
            }
            if(x.endpoints.length > 2) {
                let mpl = "mpl"+mpl_count++;
                graph.addNode(mpl, {size: 10});
                for (const e of x.endpoints) {
                graph.addEdge(e.socket.element, mpl);
                }
            }
        }

    }
    if(data.resources !== undefined) {

        for (const x of data.resources) {
            console.log("plop resource", x);
            let color = "#222";
            if (x.roles.includes("InfraSwitch")) {
                if (x.roles.includes("Spine")) {
                color = "#0000ff";
                }
                if (x.roles.includes("Stem")) {
                color = "#6D6DFF";
                }
                if (x.roles.includes("Leaf")) {
                color = "#B6B6FF";
                }
            }
            if (x.roles.includes("XpSwitch")) {
                if (x.roles.includes("Spine")) {
                color = "#008000";
                }
                if (x.roles.includes("Stem")) {
                color = "#599859";
                }
                if (x.roles.includes("Leaf")) {
                color = "#8EB78E";
                }
            }
            if (x.roles.includes("Gateway")) {
                color = "#CB0606";
            }
            if (x.roles.includes("NetworkEmulator")) {
                color = "#267E81";
            }
            if (x.roles.includes("InfraServer")) {
                color = "#822626"
            }
            if (x.roles.includes("StorageServer")) {
                color = "#7949A1"
            }

            graph.addNode(x.id, {label: x.id, size: 5, color: color});
        }

        for (const x of data.cables) {
           // handle both p2p and breakout cables
           for (const c of x.ends[1].connectors) {
               graph.addEdge(
                   x.ends[0].connectors[0].Port.element,
                   c.Port.element,
               )

           }
        }

    }

    this.doRender(graph);


    //this.node.textContent = "TESTTTTTTTTTTTTT" + JSON.stringify(data);
    
    return Promise.resolve();
  }

  doRender(graph:any) {

    console.log(this);

    randomLayout.assign(graph);

    //const container = document.getElementById("container");
    const container = this.node;
    container.innerHTML = ''; // going nuclear to kill previous renders, should be a better way ...
    console.log(container)
    const renderer = new WebGLRenderer(graph, container);
    console.log("clearing");
    renderer.clear();

    const layout = new FA2Layout(graph, { 
      settings: { 
        barnesHutOptimize: true,
        slowDown: 20,
      } 
    });

    layout.start()
    setTimeout(() => layout.stop(), 9000)


  }

  private _mimeType: string;
}

/**
 * A mime renderer factory for json data.
 */
export const rendererFactory: IRenderMime.IRendererFactory = {
  safe: true,
  mimeTypes: [MIME_TYPE],
  createRenderer: options => new OutputWidget(options)
};

/**
 * Extension definition.
 */
const extension: IRenderMime.IExtension = {
  id: 'xir:plugin',
  rendererFactory,
  rank: 0,
  dataType: 'json',
  fileTypes: [
    {
      name: 'json',
      mimeTypes: [MIME_TYPE],
      extensions: ['.json']
    }
  ],
  documentWidgetFactoryOptions: {
    name: 'XIR',
    primaryFileType: 'json',
    fileTypes: ['json'],
    defaultFor: ['json']
  }
};

export default extension;
